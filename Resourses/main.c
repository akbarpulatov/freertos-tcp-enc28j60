#include <stdio.h>
#include <time.h>

#include "stm32f1xx.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"


/* Demo application includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"

/* Simple UDP client and server task parameters. */
#define mainSIMPLE_UDP_CLIENT_SERVER_TASK_PRIORITY		( tskIDLE_PRIORITY )
#define mainSIMPLE_UDP_CLIENT_SERVER_PORT				( 5005UL )

/* Echo client task parameters - used for both TCP and UDP echo clients. */
#define mainECHO_CLIENT_TASK_STACK_SIZE 				( configMINIMAL_STACK_SIZE * 2 )	/* Not used in the Windows port. */
#define mainECHO_CLIENT_TASK_PRIORITY					( tskIDLE_PRIORITY + 1 )

/* Echo server task parameters. */
#define mainECHO_SERVER_TASK_STACK_SIZE					( configMINIMAL_STACK_SIZE * 2 )	/* Not used in the Windows port. */
#define mainECHO_SERVER_TASK_PRIORITY					( tskIDLE_PRIORITY + 1 )


/* Set the following constants to 1 or 0 to define which tasks to include and
exclude:

mainCREATE_SIMPLE_UDP_CLIENT_SERVER_TASKS:  When set to 1 two UDP client tasks
and two UDP server tasks are created.  The clients talk to the servers.  One set
of tasks use the standard sockets interface, and the other the zero copy sockets
interface.  These tasks are self checking and will trigger a configASSERT() if
they detect a difference in the data that is received from that which was sent.
As these tasks use UDP, and can therefore loose packets, they will cause
configASSERT() to be called when they are run in a less than perfect networking
environment.

mainCREATE_TCP_ECHO_TASKS_SINGLE:  When set to 1 a set of tasks are created that
send TCP echo requests to the standard echo port (port 7), then wait for and
verify the echo reply, from within the same task (Tx and Rx are performed in the
same RTOS task).  The IP address of the echo server must be configured using the
configECHO_SERVER_ADDR0 to configECHO_SERVER_ADDR3 constants in
FreeRTOSConfig.h.

mainCREATE_TCP_ECHO_SERVER_TASK:  When set to 1 a task is created that accepts
connections on the standard echo port (port 7), then echos back any data
received on that connection.
*/
#define mainCREATE_SIMPLE_UDP_CLIENT_SERVER_TASKS	1
#define mainCREATE_TCP_ECHO_TASKS_SINGLE			0
#define mainCREATE_TCP_ECHO_SERVER_TASK				0

static const uint8_t ucIPAddress[4] = { 192, 168, 233, 97 };
static const uint8_t ucNetMask[4] = { 255, 255, 255,  0 };
static const uint8_t ucGatewayAddress[4] = { 192, 168, 233, 1 };
static const uint8_t ucDNSServerAddress[4] = { 192, 168, 233, 1 };
extern uint8_t macaddr[6];


#define delaypolling(n) for(uint32_t i=0; i<n; i++){}

void GPIO_Init(void);
void vTaskLed1(void *argument);
void vTaskLed2(void *argument);
void vTaskLed3(void *argument);
	
int main(void)
{
	GPIO_Init();
	
	
	
	xTaskCreate(vTaskLed1, "LED", 32, NULL, 1, NULL);
	xTaskCreate(vTaskLed2, "LED", 32, NULL, 1, NULL);
	xTaskCreate(vTaskLed3, "LED", 32, NULL, 1, NULL);
	
	
	FreeRTOS_IPInit(ucIPAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, macaddr);
	
	vTaskStartScheduler();
	
	while(1)
	{
		
		

		
	}
}
/****************************************************************************************/
void GPIO_Init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	
	GPIOC->CRH &= ~(GPIO_CRH_CNF13 | GPIO_CRH_MODE13) ; 	
	GPIOC->CRH |= GPIO_CRH_MODE13;
	
	GPIOC->CRH &= ~(GPIO_CRH_CNF14 | GPIO_CRH_MODE14) ; 	
	GPIOC->CRH |= GPIO_CRH_MODE14;
	
	GPIOC->CRH &= ~(GPIO_CRH_CNF15 | GPIO_CRH_MODE15) ; 	
	GPIOC->CRH |= GPIO_CRH_MODE15;	
	
	
}

void vTaskLed1(void *argument)
{
	while(1)
	{
		GPIOC->BSRR = GPIO_BSRR_BS13;
		vTaskDelay(1);
		GPIOC->BSRR = GPIO_BSRR_BR13;
		vTaskDelay(1);
	}
}

void vTaskLed2(void *argument)
{
	while(1)
	{
//		delaypolling(720000);
		GPIOC->BSRR = GPIO_BSRR_BS14;
		vTaskDelay(1);
		GPIOC->BSRR = GPIO_BSRR_BR14;
		vTaskDelay(1);
	}
}

void vTaskLed3(void *argument)
{
	while(1)
	{
		GPIOC->BSRR = GPIO_BSRR_BS15;
		vTaskDelay(1);
		GPIOC->BSRR = GPIO_BSRR_BR15;
		vTaskDelay(1);
	}
}
