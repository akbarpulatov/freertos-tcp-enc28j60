/********************************************************************************
 * project															            *
 *                                                                              *
 * file			netif.c															*
 * author		Akbar Pulatov													*
 * date			01.03.2020														*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *                                                                              *
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "enc28j60.h"
#include "stm32f1xx.h"
#include "FreeRTOS_IP.h"
#include "NetworkBufferManagement.h"
#include "spi.h"

/********************************************************************************
 * User variables
 ********************************************************************************/

/********************************************************************************
 * Calls Initialization API from enc28j60.c file
 * Assumed success initialization here
 ********************************************************************************/

BaseType_t xNetworkInterfaceInitialise(void)
{
	SPIx_Init(SPI1);
	enc28j60_ini();
	return pdPASS;
}

/********************************************************************************
 * Called from tcp stack whenever the network buffer is ready to be transmitted
 * NetworkBufferDescriptor_t * const pxDescriptor
 * BaseType_t xReleaseAfterSend
 ********************************************************************************/

BaseType_t xNetworkInterfaceOutput( NetworkBufferDescriptor_t * const pxDescriptor,	
									BaseType_t xReleaseAfterSend)
{
		/* Simple network interfaces (as opposed to more efficient zero copy network
	interfaces) just use Ethernet peripheral driver library functions to copy
	data from the FreeRTOS+TCP buffer into the peripheral driver�s own buffer.
	This example assumes SendData() is a peripheral driver library function that
	takes a pointer to the start of the data to be sent and the length of the
	data to be sent as two separate parameters.  The start of the data is located
	by pxDescriptor->pucEthernetBuffer.  The length of the data is located
	by pxDescriptor->xDataLength. */
	
	enc28j60_packetSend(pxDescriptor->pucEthernetBuffer, pxDescriptor->xDataLength);
	
	/* Call the standard trace macro to log the send event. */
	iptraceNETWORK_INTERFACE_TRANSMIT();
	
	if (xReleaseAfterSend != pdFALSE)
	{
		/* It is assumed SendData() copies the data out of the FreeRTOS+TCP Ethernet
		buffer.  The Ethernet buffer is therefore no longer needed, and must be
		freed for re-use. */
		vReleaseNetworkBufferAndDescriptor(pxDescriptor);
	}
	return pdTRUE;
}

/********************************************************************************
 * 
 ********************************************************************************/

/********************************* END OF FILE **********************************/